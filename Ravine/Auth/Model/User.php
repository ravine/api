<?php

namespace Ravine\Auth\Model;

use Ravine\Project\Model\Project;
use Ravine\Project\ProjectCollection;

class User extends \Indeed\Auth\Model\User
{
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'user_project', 'userId', 'projectId');
    }

    public function getProjects() : ProjectCollection
    {
        return new ProjectCollection($this->projects);
    }
}