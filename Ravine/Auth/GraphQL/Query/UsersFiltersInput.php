<?php
namespace Ravine\Auth\GraphQL\Query;

use GraphQL\Type\Definition\InputObjectType;
use Indeed\GraphQL\Query\Filter\EqualFilter;
use Indeed\GraphQL\Query\Filter\GtFilter;
use Indeed\GraphQL\Query\Filter\LikeFilter;
use Indeed\GraphQL\Query\Filter\QueryFilterInterface;
use Ravine\Auth\Model\User;
use GraphQL\Type\Definition\Type;

class UsersFiltersInput
{
    private $filters;
    private $query;

    public function __construct()
    {
        $this->initFilters();
        $this->initQuery();
    }

    public function initFilters()
    {
        $this->filters = [
            new EqualFilter('id', Type::int(), 'id equal description'),
            new GtFilter('id', Type::int(), 'id gt description'),
            new EqualFilter('email', Type::string(), 'email equal description'),
            new LikeFilter('email', Type::string(), 'email like description'),
        ];
    }

    private function initQuery()
    {
        $this->query = User::query();
    }

    public function filter(array $rawFilters = [])
    {
        foreach ($rawFilters as $rawFilterName => $rawFilterValue) {
            /** @var QueryFilterInterface $filter */
            foreach ($this->filters as $filter) {
                if ($rawFilterName == $filter->getFilterName()) {
                    $this->query = $filter->do($this->query, $rawFilterValue);
                }
            }
        }

        return $this->query->get();
    }

    public function getDescription()
    {
        $fields = [];

        /** @var QueryFilterInterface $filter */
        foreach ($this->filters as $filter) {
            $fields[$filter->getFilterName()] = [
                'name' => $filter->getFilterName(),
                'type' => $filter->getType(),
                'description' => $filter->getDescription(),
            ];
        }

        return new InputObjectType([
            'name' => (new \ReflectionClass($this))->getShortName(),
            'fields' => $fields
        ]);
    }
}