<?php
namespace Ravine\Auth\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Ravine\Auth\GraphQL\Type\UserType;
use GraphQL\Type\Definition\InputObjectType;

class UsersQuery extends Query
{
    protected $attributes = [
        'name' => 'users'
    ];
    private $dataSource;

    public function __construct($attributes = [])
    {
        parent::__construct($attributes);

        $this->dataSource = new UsersFiltersInput();
    }

    public function type()
    {
        return Type::listOf(GraphQL::type(UserType::SLUG));
    }

    public function args()
    {
        return [
            'filters' => [
                'type' => $this->dataSource->getDescription(),
                'defaultValue' => []
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $filters = $args['filters'] ?? [];

        return $this->dataSource->filter($filters);
    }
}