<?php
namespace Ravine\Auth\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\CustomScalarType;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use Indeed\GraphQL\Type\Definition\EmailType;
use Ravine\Auth\Model\User;
use Ravine\Project\GraphQL\Type\ProjectType;

class UserType extends GraphQLType
{
    const SLUG = 'user';

    protected $attributes = [
        'name' => 'User',
        'description' => 'A user'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the user'
            ],
            'email' => [
                'type' => new EmailType(),
                'description' => 'email desc'
            ],
            'projects' => [
                'type' => Type::listOf(GraphQL::type(ProjectType::SLUG))
            ]
        ];
    }

    // If you want to resolve the field yourself, you can declare a method
    // with the following format resolve[FIELD_NAME]Field()
    protected function resolveEmailField($root, $args)
    {
        return strtolower($root->email);
    }

    protected function resolveProjectsField(User $root, $arg)
    {
        return $root->getProjects();
    }
}