<?php
namespace Ravine\Auth\Serializer;

use Indeed\Base\Serializer\BaseSerializer;
use Indeed\Base\Serializer\SerializerInterface;
use Ravine\Auth\Model\User;

class UserSerializer extends BaseSerializer implements SerializerInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

     protected function getDataFor(string $property, array $parameters = [])
     {
         $result = null;

         switch ($property) {
             case 'id':
                 $result = $this->user->getId();
                 break;
             case 'firstName':
                 $result = $this->user->getFirstName();
                 break;
             case 'lastName':
                 $result = $this->user->getLastName();
                 break;
             case 'email':
                 $result = $this->user->getEmail();
                 break;
             case 'phone':
                 $result = $this->user->getPhone();
                 break;
             case 'sex':
                 $result = $this->user->getSex();
                 break;
             case 'role':
                 $result = $this->user->getRole();
                 break;
             case 'status':
                 $result = $this->user->getStatus();
                 break;
             case 'createdDate':
                 $result = $this->user->getCreatedAt();
                 break;
         }

        return $result;
     }
}