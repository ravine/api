<?php
namespace Ravine\Project\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use Ravine\Auth\GraphQL\Type\UserType;
use Ravine\Project\Model\Project;

class ProjectType extends GraphQLType
{
    const SLUG = 'project';

    protected $attributes = [
        'name' => 'Project',
        'description' => 'A project desc'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the user'
            ],
            'slug' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'slug'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'title desc'
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'description desc'
            ],
            'users' => [
                'type' => Type::listOf(GraphQL::type(UserType::SLUG))
            ]
        ];
    }

    protected function resolveProjectsField(Project $root, $arg)
    {
        return $root->getUsers();
    }
}