<?php
namespace Ravine\Project\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Ravine\Project\GraphQL\Type\ProjectType;
use Ravine\Project\Model\Project;

class ProjectsQuery extends Query
{
    protected $attributes = [
        'name' => 'users'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type(ProjectType::SLUG));
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string()
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string()
            ],
        ];
    }

    public function resolve($root, $args)
    {
        return Project::all();
    }
}