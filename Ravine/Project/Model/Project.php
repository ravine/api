<?php

namespace Ravine\Project\Model;

use Indeed\Base\Model\BaseModel;
use Ravine\Auth\Collection\UserCollection;
use Ravine\Auth\Model\User;

class Project extends BaseModel
{
    protected $table = 'project';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_project', 'projectId', 'userId');
    }

    public function getUsers() : UserCollection
    {
        return new UserCollection($this->users);
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}