<?php
namespace Ravine\Seed;

use Illuminate\Database\Seeder;
use Ravine\Auth\Model\User;
use Ravine\Project\Model\Project;

class CreateTestUsersWithProjectsSeeder extends Seeder
{
    private $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $faker)
    {
        $this->faker = $faker;

        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_DELETED);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_ACTIVE);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_BLOCKED);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_DELETED);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_ACTIVE);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_BLOCKED);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_DELETED);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_ACTIVE);
        $this->addUser(User::$ROLE_ADMIN, User::$STATUS_BLOCKED);

        $this->addProject();
        $this->addProject();
        $this->addProject();
        $this->addProject();
        $this->addProject();
        $this->addProject();
        $this->addProject();

        $projects = Project::all();

        foreach (User::all() as $user) {
            $userProject = [];
            foreach ($projects as $project) {
                if ($this->faker->boolean(30)) {
                    $userProject []= $project->getId();
                }
            }

            $user->projects()->sync($userProject);
        }
    }

    private function addUser($role, $status)
    {
        $user = new User();
        $user->setFirstName($this->faker->firstName);
        $user->setLastName($this->faker->lastName);
        $availableSex = User::getAvailableSex();
        shuffle($availableSex);
        $user->setSex($availableSex[0]);
        $user->setEmail($this->faker->email);
        $user->setPhone($this->faker->phoneNumber);
        $user->setPassword($this->faker->password);
        $user->setRole($role);
        $user->setStatus($status);

        $user->save();
    }

    private function addProject()
    {
        $project = new Project();
        $project->setTitle($this->faker->domainName);
        $project->setSlug($this->faker->slug);
        $project->setDescription($this->faker->sentence);
        $statuses = Project::getAvailableStatuses();
        shuffle($statuses);
        $project->setStatus($statuses[0]);

        $project->save();
    }
}
