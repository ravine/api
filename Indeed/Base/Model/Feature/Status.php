<?php
namespace Indeed\Base\Model\Feature;

trait Status
{
    static $STATUS_ACTIVE = 1;
    static $STATUS_BLOCKED = 2;
    static $STATUS_DELETED = 3;

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setStatus(int $status)
    {
        if (!in_array($status, static::getAvailableStatuses())) {
            throw new \Exception('Status ' . $status . ' is not supported');
        }

        $this->status = $status;
    }

    public static function getAvailableStatuses() : array
    {
        return static::getAvailableOptionsStartingFrom('STATUS_');
    }

    public static function getDeletedStatus() : int
    {
        return static::$STATUS_DELETED;
    }

    public static function getActiveStatus() : int
    {
        return static::$STATUS_ACTIVE;
    }

    public function scopeFilterOutDeleted($query)
    {
        return $query->where(self::getTableName() . '.status' , '!=', self::$STATUS_DELETED);
    }
}