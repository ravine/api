<?php
namespace Indeed\Base\Model\Feature;

trait AvailableOptions
{
    protected static function getAvailableOptionsStartingFrom(string $propertyPrefix) : array
    {
        $reflection = new \ReflectionClass(static::class);
        $allProperties = $reflection->getStaticProperties();
        $options = array_filter($allProperties, function($propertyName) use ($propertyPrefix) {
            return preg_match('|^' . $propertyPrefix  . '|', $propertyName);
        }, ARRAY_FILTER_USE_KEY);

        return array_values($options);
    }
}