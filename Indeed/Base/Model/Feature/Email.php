<?php
namespace Indeed\Base\Model\Feature;

trait Email
{
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }
}