<?php
namespace Indeed\Base\Model\Feature;

trait Id
{
    public function getId() : int
    {
        return $this->id;
    }
}