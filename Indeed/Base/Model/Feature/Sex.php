<?php
namespace Indeed\Base\Model\Feature;

trait Sex
{
    static $SEX_FEMALE = 1;
    static $SEX_MALE = 2;

    public function getSex() : int
    {
        return $this->sex;
    }

    public function setSex(int $sex)
    {
        if (!in_array($sex, static::getAvailableSex())) {
            throw new \Exception('Sex ' . $sex . ' is not supported');
        }

        $this->sex = $sex;
    }

    public static function getAvailableSex() : array
    {
        return static::getAvailableOptionsStartingFrom('SEX_');
    }
}