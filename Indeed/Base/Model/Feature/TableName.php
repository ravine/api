<?php
namespace Indeed\Base\Model\Feature;

trait TableName
{
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}