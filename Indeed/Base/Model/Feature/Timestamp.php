<?php
namespace Indeed\Base\Model\Feature;

trait Timestamp
{
    public function getCreatedAt() : \DateTime
    {
        return $this->created_at;
    }

    public function getUpdatedAt() : \DateTime
    {
        return $this->updated_at;
    }
}