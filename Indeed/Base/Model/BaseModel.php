<?php
namespace Indeed\Base\Model;

use Illuminate\Database\Eloquent\Model;
use Indeed\Base\Model\Feature;

class BaseModel extends Model implements ModelWithStatusInterface
{
    use Feature\Id;
    use Feature\Timestamp;
    use Feature\Status;
    use Feature\TableName;
    use Feature\AvailableOptions;
}