<?php
namespace Indeed\Base\Model;

interface ModelWithStatusInterface
{

    public function getStatus() : int;
    public function setStatus(int $status);
    public static function getAvailableStatuses() : array;
    public static function getDeletedStatus() : int;
    public static function getActiveStatus() : int;
}