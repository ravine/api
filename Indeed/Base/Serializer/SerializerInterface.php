<?php
namespace Indeed\Base\Serializer;

interface SerializerInterface
{
    public function serialize(array $parameters = []) : array;
}