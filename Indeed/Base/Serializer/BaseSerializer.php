<?php
namespace Indeed\Base\Serializer;

abstract class BaseSerializer implements SerializerInterface
{
    public function serialize(array $parameters = []) : array
    {
        $serializedData = [];

        foreach ($parameters as $key => $value) {
            if (is_array($value)) {
                $serializedData[$key] = $this->getDataFor($key, $value);
            } else if (!is_array($value)){
                $serializedData[$value] = $this->getDataFor($value);
            }
        }

        return $serializedData;
    }

    abstract protected function getDataFor(string $key, array $parameters = []);
}