<?php
namespace Indeed\GraphQL\Query\Filter;

use Illuminate\Database\Eloquent\Builder;

class EqualFilter extends BaseFilter implements QueryFilterInterface
{
    public function do(Builder $query, $value): Builder
    {
        return $query->where($this->filterName , '=', $value);
    }

    protected function getFilterType(): string
    {
        return 'equal';
    }
}