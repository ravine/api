<?php
namespace Indeed\GraphQL\Query\Filter;

use GraphQL\Type\Definition\ScalarType;
use Illuminate\Database\Eloquent\Builder;

interface QueryFilterInterface
{
    public function do(Builder $query, $value) : Builder;

    public function getFilterName() : string;
    public function getType() : ScalarType;
    public function getDescription() : string;
}