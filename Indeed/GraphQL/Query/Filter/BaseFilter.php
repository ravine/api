<?php
namespace Indeed\GraphQL\Query\Filter;

use GraphQL\Type\Definition\ScalarType;

abstract class BaseFilter
{
    protected $filterName;
    protected $type;
    protected $description;

    public function __construct(string $filterName, ScalarType $type, string $description)
    {
        $this->filterName = $filterName;
        $this->type = $type;
        $this->description = $description;
    }

    public function getFilterName(): string
    {
        return $this->filterName . '_' . $this->getFilterType();
    }

    abstract protected function getFilterType() : string;

    public function getType(): ScalarType
    {
        return $this->type;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}