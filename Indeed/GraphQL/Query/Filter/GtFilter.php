<?php
namespace Indeed\GraphQL\Query\Filter;

use Illuminate\Database\Eloquent\Builder;

class GtFilter extends BaseFilter implements QueryFilterInterface
{
    public function do(Builder $query, $value): Builder
    {
        return $query->where($this->filterName , '>', $value);
    }

    protected function getFilterType(): string
    {
        return 'gt';
    }
}