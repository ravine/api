<?php

namespace Indeed\Auth\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use Indeed\Base\Model\BaseModel;
use Indeed\Base\Model\Feature;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;

    use Feature\NameExtended;
    use Feature\Email;
    use Feature\Sex;

    static $ROLE_CREATOR = 1986;
    static $ROLE_ADMIN = 1;
    static $ROLE_USER = 2;

    protected $table = 'user';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function getRole() : string
    {
        return $this->role;
    }

    public function setRole(string $role)
    {
        $this->role = $role;
    }

    public static function getAvailableRoles() : array
    {
        return static::getAvailableOptionsStartingFrom('ROLE_');
    }

    public static function getAvailableRolesWithoutCreator() : array
    {
        return array_diff(self::getAvailableRoles(), [self::$ROLE_CREATOR]);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function isAdmin() : bool
    {
        return in_array($this->getRole(), [self::$ROLE_ADMIN, self::$ROLE_CREATOR]);
    }
}
