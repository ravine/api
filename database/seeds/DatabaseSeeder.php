<?php

use Illuminate\Database\Seeder;
use Ravine\Seed\CreateTestUsersWithProjectsSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CreateTestUsersWithProjectsSeeder::class);
    }
}
