<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTableAndUserProjectRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('description');
            $table->smallInteger('status');
            $table->timestamps();
        });

        Schema::create('user_project', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('userId')->unsigned();
            $table->foreign('userId')->references('id')->on('user');

            $table->integer('projectId')->unsigned();
            $table->foreign('projectId')->references('id')->on('project');

            $table->integer('role')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_project');
        Schema::dropIfExists('project');
    }
}
