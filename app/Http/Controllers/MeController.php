<?php
namespace App\Http\Controllers;

use Ravine\Auth\Serializer\UserSerializer;
use Illuminate\Http\Request;

class MeController extends Controller
{
    public function index(Request $request)
    {
        $parameters = $request->all() ? json_decode($request->all()[0], true) : [];
        $responseDataFormat = $parameters['responseDataFormat'] ?? [];

        // var_dump($responseDataFormat); exit;

        $userSerializer = new UserSerializer(\JWTAuth::toUser());
        return $userSerializer->serialize($responseDataFormat);
    }
}